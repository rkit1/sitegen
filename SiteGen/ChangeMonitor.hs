{-# LANGUAGE DeriveFunctor, DeriveDataTypeable, TypeOperators, FlexibleContexts
  , ScopedTypeVariables, MultiParamTypeClasses, FunctionalDependencies
  , FlexibleInstances, UndecidableInstances, OverlappingInstances #-}
module SiteGen.ChangeMonitor where
import System.FSNotify
import Control.Concurrent.Chan
import Data.String
import Data.Typeable
import Control.Monad.Trans
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import qualified Data.Set as S
import qualified Filesystem.Path.CurrentOS as FP
import qualified Control.Exception as E
import Control.Monad
import Control.Concurrent
import SiteGen.Queue
import qualified Data.Foldable as F
import Control.Monad.Catch

-- | private
newtype ChangeMonitor = CM (TVar (S.Set String))

withManager1 :: (MonadIO m, MonadMask m) => (WatchManager -> m c) -> m c
withManager1 = bracket (liftIO startManager) (liftIO . stopManager)


withChangeMonitor :: (MonadIO m, MonadMask m)
  => String -> (ChangeMonitor -> m b) -> m b
withChangeMonitor path f = do
  v <- liftIO $ atomically $ newTVar $ S.empty
  withManager1 $ \ mgr -> do
    liftIO $ watchTree mgr (FP.decodeString path) (const True)
      $ \ e -> atomically $ modifyTVar v $ S.insert $ FP.encodeString $ eventPath e
    f $ CM v

collectChanges :: MonadIO m => ChangeMonitor -> m (S.Set String)
collectChanges (CM v) = liftIO $ atomically $ do
  a <- readTVar v
  when (S.null a) retry
  writeTVar v S.empty
  return a

collectAndEnqueueChanges :: (MonadIO m, MonadQueue di m, Typeable di) =>
  ChangeMonitor -> (FilePath -> m [di]) -> m ()
collectAndEnqueueChanges cm f = do
  cs <- collectChanges cm
  F.forM_ cs $ \ fp -> f fp >>= forceEnqueueF

