{-# LANGUAGE TemplateHaskell #-}
module SiteGen.Helpers where
import Language.Haskell.TH
import Control.Monad.Catch
import Control.Monad.Trans
import Control.Applicative

mkNewtypeMonad :: String -> DecsQ
mkNewtypeMonad t = do
  m <- newName "m"
  a <- newName "a"
  let
    tN = mkName t
    tT = conT tN
    tE = conE tN
    runN = mkName ("run" ++ t)
    runE = varE runN
    newtypeDec = NewtypeD [] tN [PlainTV m, PlainTV a] 
                 (RecC tN [(runN, NotStrict, AppT (VarT m) (VarT a))])
                 [''Monad, ''Functor, ''Applicative, ''MonadIO]
  decs <- [d|
    instance MonadTrans $tT where
      lift = $tE
  
    instance (Monad m, MonadThrow m) => MonadThrow ($tT m) where
      throwM e = $tE (throwM e)
    instance MonadCatch m => MonadCatch ($tT m) where
      catch m f = $tE (catch ($runE m) ($runE . f))
    instance MonadMask m => MonadMask ($tT m) where
      mask a = $tE $ mask $ \u -> $runE (a $ q u)
        where q u = $tE . u . $runE
      uninterruptibleMask a =
        $tE $ uninterruptibleMask $ \u -> $runE (a $ q u)
          where q u = $tE . u . $runE
    |]
  return $ newtypeDec : decs



mkNewtypeMonadNoTrans :: String -> DecsQ
mkNewtypeMonadNoTrans t = do
  m <- newName "m"
  a <- newName "a"
  let
    tN = mkName t
    tT = conT tN
    tE = conE tN
    runN = mkName ("run" ++ t)
    runE = varE runN
    newtypeDec = NewtypeD [] tN [PlainTV m, PlainTV a] 
                 (RecC tN [(runN, NotStrict, AppT (VarT m) (VarT a))])
                 [''Monad, ''Functor, ''Applicative, ''MonadIO]
  decs <- [d|
    instance (Monad m, MonadThrow m) => MonadThrow ($tT m) where
      throwM e = $tE (throwM e)
    instance MonadCatch m => MonadCatch ($tT m) where
      catch m f = $tE (catch ($runE m) ($runE . f))
    instance MonadMask m => MonadMask ($tT m) where
      mask a = $tE $ mask $ \u -> $runE (a $ q u)
        where q u = $tE . u . $runE
      uninterruptibleMask a =
        $tE $ uninterruptibleMask $ \u -> $runE (a $ q u)
          where q u = $tE . u . $runE
    |]
  return $ newtypeDec : decs
