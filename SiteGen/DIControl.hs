{-# LANGUAGE MultiParamTypeClasses, RecordWildCards, FlexibleInstances, TypeOperators
  , GeneralizedNewtypeDeriving, FunctionalDependencies, UndecidableInstances
  , TemplateHaskell, DeriveDataTypeable, DeriveFunctor, OverlappingInstances
  , FlexibleContexts, ScopedTypeVariables, BangPatterns
  #-}
module SiteGen.DIControl where
import Control.Monad.Trans
import Control.Monad


class DIControl di t m | m -> di t where
  doesExistDI :: di -> m Bool
  checkTimeDI :: di -> m t
  listDI :: m [di]
  removeDI :: di -> m ()

instance (DIControl di t m, MonadTrans m', Monad m) => DIControl di t (m' m) where
  doesExistDI = lift . doesExistDI
  checkTimeDI = lift . checkTimeDI
  listDI = lift listDI
  removeDI = lift . removeDI


safeCheckTimeDI :: (DIControl di t m, Monad m) => di -> m (Maybe t)
safeCheckTimeDI di = do
  c <- doesExistDI di
  case c of
    True -> Just `liftM` checkTimeDI di
    False -> return Nothing
