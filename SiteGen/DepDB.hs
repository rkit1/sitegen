{-# OPTIONS_GHC -fno-warn-deprecated-flags #-}
{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances
  , GeneralizedNewtypeDeriving, TypeOperators, FlexibleContexts, BangPatterns
  , UndecidableInstances, OverlappingInstances, DeriveDataTypeable, DeriveFunctor
  , DatatypeContexts #-}
module SiteGen.DepDB where
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Control.Monad.State.Strict 
import Control.Applicative
import Data.Typeable

data (Ord si, Ord di) => DepDBRecord si di t
  = DepDBRecord
    { ddr_result :: Bool
    , ddr_ss :: !(M.Map si t)
    , ddr_ds :: !(S.Set di) }
    deriving (Typeable, Show)

class DepDB si di t m | m -> si di t where
  recordDeps :: di -> DepDBRecord si di t -> m ()
  lookupDeps :: di -> m (Maybe (DepDBRecord si di t))

instance (Monad m, Ord di) =>
         DepDB si di t (StateT (M.Map di (DepDBRecord si di t)) m) where
  recordDeps di ddr = modify (M.insert di ddr)
  lookupDeps di = gets $ M.lookup di

instance (DepDB si di t m', MonadTrans m, Monad m') => DepDB si di t (m m') where
  recordDeps di ddr = lift $ recordDeps di ddr
  lookupDeps di = lift $ lookupDeps di

runSimpleDepDB
  :: Monad m
     => Maybe (M.Map di (DepDBRecord si di t))
     -> StateT (M.Map di (DepDBRecord si di t)) m a
     -> m (a, M.Map di (DepDBRecord si di t))
runSimpleDepDB Nothing m = runStateT m M.empty
runSimpleDepDB (Just a) m = runStateT m a

-----
-- OPTIONAL
-----
{-
class DepDB si di t m => DepDBLookupSI si di t m | m -> si di t where
  lookupDepsBySI :: si -> m [(di, DepDBRecord si di t)]

instance (Monad m, Ord di, Ord si) =>
         DepDBLookupSI si di t (StateT (M.Map di (DepDBRecord si di t)) m) where
  lookupDepsBySI si = do
    list <- gets M.toList
    return [ (di, r)
           | (di, r) <- list
           , M.member si $ ddr_ss r ]

instance (DepDBLookupSI si di t m', MonadTrans m, Monad m') =>
         DepDBLookupSI si di t (m m') where
  lookupDepsBySI si = lift $ lookupDepsBySI si
-}
