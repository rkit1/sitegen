{-# LANGUAGE ExistentialQuantification, MultiParamTypeClasses, BangPatterns
  , FunctionalDependencies, FlexibleInstances, TypeFamilies, UndecidableInstances
  , GeneralizedNewtypeDeriving, TemplateHaskell, FlexibleContexts
  , DeriveDataTypeable, ScopedTypeVariables, TypeOperators
  , DeriveFunctor, OverlappingInstances, RecordWildCards  #-}
module SiteGen.DepRecord where
import SiteGen.Helpers
import Control.Monad.Trans
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Typeable
import Control.Monad.State.Strict
import Data.Monoid
import Data.Functor
import Control.Applicative
import Control.Monad.Catch
import SiteGen.IO
import qualified Data.Set as S

-- * Check version of source
class SIVersion si v m | m -> si v where
  getSiVersion :: si -> m v
  isSiChanged :: si -> v -> m Bool

instance (SIVersion si v m', Monad m', MonadTrans m)
         => SIVersion si v (m m') where
  getSiVersion si = lift $ getSiVersion si
  isSiChanged si v = lift $ isSiChanged si v


-- * Keep track of deps while processing destination

class Monad m => DepRecord si di m | m -> si di where
  recordSI :: si -> m ()
  getRecording :: m Bool
  setRecording :: Bool -> m ()

instance (Monad (m m'), DepRecord si di m', Monad m', MonadTrans m)
         => DepRecord si di (m m') where
  recordSI = lift . recordSI
  getRecording = lift getRecording
  setRecording = lift . setRecording

data DepRecordState si di v =
  DepRecordState { siDeps :: !(M.Map si v)
                 , isRecording :: Bool }

type DepRecordM si di v = StateT (DepRecordState si di v)

instance (Ord si, Monad m, SIVersion si v m)
         => DepRecord si di (DepRecordM si di v m) where
  recordSI si = do
    v <- getSiVersion si
    st@DepRecordState{..} <- get
    when isRecording $ put st{siDeps = M.insertWith (curry fst) si v siDeps}
  getRecording = gets isRecording
  setRecording c = do
    modify $ \ st@DepRecordState{..} -> st{isRecording = c}

instance (SiteI si m, SIVersion si v m) => SiteI si (DepRecordM si di v m) where
  openSI si = do
    recordSI si
    lift $ openSI si
  doesExistSI si = do
    recordSI si
    lift $ doesExistSI si

runDepRecord :: (SiteI si m, SIVersion si v m) =>
  DepRecordM si di v m a -> m (a, DepRecordState si di v)
runDepRecord m = runStateT m (DepRecordState M.empty True)




-- * peek

peek :: (DepRecord si di m) => m a -> m a
peek m = do
  r <- getRecording
  setRecording False
  a <- m
  setRecording r
  return a
