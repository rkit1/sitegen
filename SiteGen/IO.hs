{-# LANGUAGE MultiParamTypeClasses, RecordWildCards, FlexibleInstances, TypeOperators
  , GeneralizedNewtypeDeriving, FunctionalDependencies, UndecidableInstances
  , TemplateHaskell, DeriveDataTypeable, DeriveFunctor, OverlappingInstances
  , FlexibleContexts, ScopedTypeVariables, BangPatterns, TemplateHaskell
  #-}
module SiteGen.IO where
import SiteGen.Helpers
import Library
import System.IO
import SiteGen.DepDB
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Map as M
import Control.Applicative
import qualified Data.Set as S
import Data.Typeable
import Language.Haskell.TH
import Control.Monad.Trans
import Control.Monad
import Data.Time.Clock
import System.Directory
import System.FilePath

class (MonadIO m, Ord si) => SiteI si m | m -> si where
  openSI :: si -> m Handle
  doesExistSI :: si -> m Bool


class (SiteI si m, Ord di) => SiteIO si di m | m -> si di where
  openDI :: di -> m Handle
  copySItoDI :: si -> di -> m ()
  copySItoDI si di = do
    hSI <- openSI si
    hDI <- openDI di
    liftIO $ LBS.hGetContents hSI >>= LBS.hPut hDI

instance (SiteI si m', MonadIO (m m'), MonadTrans m)
         => SiteI si (m m') where
  openSI = lift . openSI
  doesExistSI = lift . doesExistSI

instance (SiteIO si di m', MonadIO (m m'), MonadTrans m)
         => SiteIO si di (m m') where
  openDI = lift . openDI
  copySItoDI si di = lift $ copySItoDI si di 

readString :: (SiteI si m) => si -> m String
readString si = openSI si >>= liftIO . hGetContents

readByteString :: (SiteI si m) => si -> m BS.ByteString
readByteString si = openSI si >>= liftIO . BS.hGetContents

readByteStringL :: (SiteI si m) => si -> m LBS.ByteString
readByteStringL si = openSI si >>= liftIO . LBS.hGetContents

writeString :: SiteIO si di m => di -> String -> m ()
writeString di str = do
  openDI di >>= liftIO . flip hPutStr str

writeByteString :: SiteIO si di m => di -> BS.ByteString -> m ()
writeByteString di str = do
  openDI di >>= liftIO . flip BS.hPutStr str

writeByteStringL :: SiteIO si di m => di -> LBS.ByteString -> m ()
writeByteStringL di str = do
  openDI di >>= liftIO . flip LBS.hPutStr str

-- * simple SiteIO

$(mkNewtypeMonad "SimpleIO")


instance (MonadIO m) => SiteI FilePath (SimpleIO m) where
  openSI fp = liftIO $ do
    h <- openFile fp ReadMode
    hSetEncoding h utf8
    return h
  doesExistSI fp = liftIO $ doesFileExist fp

instance (MonadIO m) => SiteIO FilePath FilePath (SimpleIO m) where
  openDI fp = liftIO $ do
    createDirectoryIfMissing True $ takeDirectory fp
    h <- openFile fp WriteMode
    hSetEncoding h utf8
    return h
  copySItoDI sfp dfp = liftIO $ do
    createDirectoryIfMissing True $ takeDirectory dfp
    copyFile sfp dfp
