{-# LANGUAGE ScopedTypeVariables, BangPatterns, MultiParamTypeClasses, TypeOperators
  , FunctionalDependencies, GeneralizedNewtypeDeriving, FlexibleInstances
  , UndecidableInstances, TypeFamilies, ExistentialQuantification
  , OverlappingInstances, TemplateHaskell
  #-}
module SiteGen.Process where
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import SiteGen.DepDB
import SiteGen.DepRecord
import SiteGen.IO
import SiteGen.DIControl
import Control.Monad.State.Strict
import Control.Monad.Writer.Strict
import SiteGen.Queue
import Data.Typeable
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import qualified Data.Foldable as F
import Control.Monad.Reader
import SiteGen.Helpers

$(mkNewtypeMonad "DIM_")
type DIM di m = DIM_ (StateT (di, S.Set di) m)

runDIM :: Monad m => di -> DIM di m a -> m (a, S.Set di)
runDIM di m = do
  (a, (di, dd)) <- runStateT (runDIM_ m) (di, S.empty)
  return (a, dd)

class DIContext di m | m -> di where
  askDI :: m di
  recordDI :: di -> m ()
instance (Monad m, Ord di) => DIContext di (DIM di m) where
  askDI = DIM_ $ gets fst
  recordDI di = DIM_ $ modify $ \ (a, snd) -> (a, S.insert di snd)
instance (DIContext di m', MonadTrans m, Monad m') => DIContext di (m m') where
  askDI = lift askDI
  recordDI di = lift $ recordDI di


runDI :: (Monad m, Ord di)
  => di -> DIM di m Bool -> m (S.Set di)
runDI di f = do
  (res, dd) <- runDIM di f
  return dd

runDIWithRebuildChecks
  :: (SIVersion si v m, SiteI si m, DepDB si di v m, Ord di)
  => di
     -> DepRecordM si di v (DIM di m) Bool
     -> m (S.Set di)
runDIWithRebuildChecks di f = do
  r <- runMaybeT $ do
    ddr <- MaybeT $ lookupDeps di

    -- is previous build succeded?
    guard $ ddr_result ddr

    -- are all dependencies unchanged?
    forM_ (M.toList $ ddr_ss ddr) $ \ (si, v) -> MaybeT $ do
      c <- isSiChanged si v
      return $ guard (not c)

    return (ddr_ds ddr)
       
  case r of
   Just a -> return a
   Nothing -> runDIWithRebuildChecksForce di f


runDIWithRebuildChecksForce
  :: (SIVersion si v m, SiteI si m, DepDB si di v m, Ord di)
  => di
     -> DepRecordM si di v (DIM di m) Bool
     -> m (S.Set di)
runDIWithRebuildChecksForce di f = do
  ((res, DepRecordState{siDeps = ss}), dd) <- runDIM di $ do
    runDepRecord f
  recordDeps di $ DepDBRecord res ss dd
  return dd
  

process
  :: (F.Foldable f, Ord di, Monad m)
  => f di
     -- ^ Initial destinations.
     
     -> (di -> m (S.Set di))
     -- ^ Actual processing goes here.
     
     -> m ()
process i f = runQueue $ enqueueF i >> go
  where
    go = do
      e <- dequeue
      case e of
        Nothing -> return ()
        Just di -> do
          ds <- lift $ f di
          enqueueF ds
          go
{-
processForever ::
  ( Ord di, Monad m, DepDB si di t m, DIControl di t m, SiteI si m
  , F.Foldable f) 
  => f di
  -- ^ Initial destinations
  
  -> m (f di)
  -- ^ Aquire some destinations
  
  -> DIM si di m Bool
  -- ^ Actual processing goes here. Return True if build is succeded,
  -- False otherwise
  
  -> m void
processForever i c f = runQueue $ do
  enqueueF i
  forever $ do
    e <- dequeue
    case e of
      Nothing -> lift c >>= enqueueF
      Just dst -> do
        ds <- lift $ runDI f dst
        enqueueF ds

-}

{-
checkForChanges :: (SiteI si t m, DIControl di t m, Ord di)
  => di -> DepDBRecord si di t -> MaybeT m ()
checkForChanges di ddr = do
  guard $ ddr_result ddr
  checkForChangesSI ddr
  checkForChangesDI di ddr


checkForChangesSI :: (SiteI si a m, Ord di)
  => DepDBRecord si di a -> MaybeT m ()
checkForChangesSI ddr = forM_ (S.toList $ ddr_ss ddr) $ \ si -> do
  t <- MaybeT $ safeCheckSI si
  guard $ t /= ddr_t ddr


checkForChangesDI :: (DIControl di t m, Ord di, Ord si, Eq t, Monad m)
  => di -> DepDBRecord si di t -> MaybeT m ()
checkForChangesDI di ddr = do
  t <- MaybeT $ safeCheckTimeDI di
  guard $ t /= ddr_t ddr
-}

{-
-- | garbage collection
collectOrpanDIs
  :: ( DIControl di t m, Ord di, Ord si, Eq t, Monad m
     , DepDB si di t m, MonadIO m, Show di)
  => m ()
collectOrpanDIs = do
  ds <- listDI
  forM_ ds $ \ di -> do
    c <- lookupDeps di
    case c of
      Just _ -> return ()
      Nothing -> do
        liftIO $ putStrLn $ "removing " ++ (show di)
        removeDI di               


-}
