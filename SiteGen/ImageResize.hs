{-# LANGUAGE DeriveDataTypeable, RecordWildCards, FlexibleContexts, CPP #-}
module SiteGen.ImageResize (resizeImage, ResizeSpec(..)) where
import Text.Printf
import Data.Typeable
import System.Process
import Control.Monad.Trans
import System.IO
import Control.Concurrent
import System.Exit
import SiteGen.IO


data ResizeSpec
  = Scale      { width :: Int, height :: Int }
  | ScaleUp    { width :: Int, height :: Int }
  | ScaleDown  { width :: Int, height :: Int }
  | ScaleFill  { width :: Int, height :: Int }
  | Resize     { width :: Int, height :: Int }
  | Percents   { count :: Int }
  | PixelCount { count :: Int }
    deriving (Show, Read, Eq, Ord, Typeable)



resizeImage :: (SiteIO si di m) => si -> di -> ResizeSpec -> String -> m ()
resizeImage inp out rs format = do
  ih <- openSI inp
  oh <- openDI out
  let
    pr = (shell $ printf "c:/cygwin64/bin/convert - -resize %s %s:-"
           (concatMap escF $ toCmdLine rs)
           format)
      { std_in  = UseHandle ih
      , std_out = UseHandle oh
      , std_err = Inherit }
  (_, _, _, ph) <- liftIO $ createProcess pr
  ExitSuccess <- liftIO $ waitForProcess ph -- fixme
  return ()



#ifdef mingw32_HOST_OS
escF '<' = "^<"
escF '>' = "^>"
escF '^' = "^^"
#else
escF '<' = "\\<"
escF '>' = "\\>"
#endif
escF x = [x]

toCmdLine Scale{..}      = printf "%dx%d"  width height
toCmdLine ScaleUp{..}    = printf "%dx%d<" width height
toCmdLine ScaleDown{..}  = printf "%dx%d>" width height
toCmdLine ScaleFill{..}  = printf "%dx%d^" width height
toCmdLine Resize{..}     = printf "%dx%d!" width height
toCmdLine Percents{..}   = printf "%d%%" count
toCmdLine PixelCount{..} = printf "%d@"  count



{-
test :: IO ()
test = runDebugIO $ do
  resizeImage "SiteGen/1.jpg" "SiteGen/1s.jpg" (ScaleDown 500 90) "jpg"
-}
