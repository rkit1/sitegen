{-# LANGUAGE DeriveFunctor, DeriveDataTypeable, BangPatterns, RecordWildCards
  , TypeOperators, FlexibleContexts, MultiParamTypeClasses, FunctionalDependencies
  , FlexibleInstances, UndecidableInstances, OverlappingInstances  #-}
module SiteGen.Queue where
import qualified Data.Set as S
import Control.Monad.State.Strict
import Data.Foldable as F

-- * Simple state monad representing process-once queue.

data QS a = QS
  { done :: !(S.Set a)
  , queue :: !(S.Set a) }

instance (MonadQueue e m, MonadTrans m', Monad m) => MonadQueue e (m' m) where
  enqueue = lift . enqueue
  dequeue = lift dequeue
  setDone b e = lift $ setDone b e

class MonadQueue e m | m -> e where
  enqueue :: e -> m ()
  dequeue :: m (Maybe e)
  setDone :: Bool -> e -> m ()
  
enqueueF :: (MonadQueue a m, Foldable t, Monad m) => t a -> m ()
enqueueF es = F.forM_ es enqueue

forceEnqueueF :: (MonadQueue e m, Foldable t, Monad m) => t e -> m ()
forceEnqueueF es = F.forM_ es $ \ e -> do
  setDone False e
  enqueue e


runQueue :: Monad m => StateT (QS e) m a -> m a
runQueue m = evalStateT m (QS S.empty S.empty)

instance (Ord e, Monad m) => MonadQueue e (StateT (QS e) m) where
  enqueue e = modify $ \ s@QS{..} -> let
    q' | S.member e done = queue
       | otherwise = S.insert e queue
    in s{queue = q'}
  dequeue = do
    s@QS{..} <- get
    let 
      (s', e) | Just (e, q) <- S.minView queue =
                  (QS{queue = q, done = S.insert e done}, Just e)
              | otherwise = (s, Nothing)
    put s'
    return e
  setDone c e = modify $ \ s@QS{..} -> let
    d' | c = S.insert e done
       | otherwise = S.delete e done
    in s{done = d'}
    
    

{-
test :: IO ()
test = runQueue f
  where
    loop = do
      a <- dequeue
      case a of Nothing -> return ()
                Just a -> lift (putStrLn a) >> loop

    f = do
      enqueue "hi"
      enqueue "hi2"
      loop
      enqueue "hi2"
      loop
      return ()
-}
